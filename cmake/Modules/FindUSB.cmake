# - Try to find USB
# Once done this will define
#
#  USB_FOUND - system has USB
#  USB_INCLUDE_DIRS - the USB include directory
#  USB_LIBRARIES - Link these to use USB
#  USB_DEFINITIONS - Compiler switches required for using USB
#
#  Copyright (c) 2009 Andreas Schneider <mail@cynapses.org>
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#  For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#

if (LIBUSB_LIBRARIES AND LIBUSB_INCLUDE_DIRS)
  # in cache already
  set(LIBUSB_FOUND TRUE)
elseif (APPLE)
  # use system libraries
  set(LIBUSB_FOUND FALSE)
else (LIBUSB_LIBRARIES AND LIBUSB_INCLUDE_DIRS)
  find_path(LIBUSB_INCLUDE_DIR
    NAMES
      usb.h
    PATHS
        /usr/include
        /usr/local/include
        /opt/local/include
        /sw/include
        /usr/include/gdal
        /usr/local/include/gdal
        /opt/local/include/gdal
        /sw/include/gdal

  )

  # debian uses version suffixes
  # add suffix evey new release
  find_library(LIBUSB_LIBRARY
    NAMES
        usb
    PATHS
      /usr/lib
      /usr/lib64
      /usr/local/lib
      /opt/local/lib
      /sw/lib
  )

  set(LIBUSB_INCLUDE_DIRS
    ${LIBUSB_INCLUDE_DIR}
  )
  set(LIBUSB_LIBRARIES
    ${LIBUSB_LIBRARY}
  )

  if (LIBUSB_INCLUDE_DIRS AND LIBUSB_LIBRARIES)
     set(LIBUSB_FOUND TRUE)
  endif (LIBUSB_INCLUDE_DIRS AND LIBUSB_LIBRARIES)

  if (LIBUSB_FOUND)
    if (NOT LIBUSB_FIND_QUIETLY)
      message(STATUS "Found LIBUSB: ${LIBUSB_LIBRARIES}")
    endif (NOT LIBUSB_FIND_QUIETLY)
  else (LIBUSB_FOUND)
    if (LIBUSB_FIND_REQUIRED)
      message(FATAL_ERROR "Could not find LIBUSB")
    endif (LIBUSB_FIND_REQUIRED)
  endif (LIBUSB_FOUND)

  # show the LIBUSB_INCLUDE_DIRS and LIBUSB_LIBRARIES variables only in the advanced view
  mark_as_advanced(LIBUSB_INCLUDE_DIRS LIBUSB_LIBRARIES)

endif (LIBUSB_LIBRARIES AND LIBUSB_INCLUDE_DIRS)

