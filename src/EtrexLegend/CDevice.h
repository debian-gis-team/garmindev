/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#ifndef CDEVICE_H
#define CDEVICE_H

#include "IDeviceDefault.h"
#include "CSerial.h"

namespace EtrexLegend
{

    class CDevice : public Garmin::IDeviceDefault
    {
        public:
            CDevice();
            virtual ~CDevice();

            std::string devname;
            uint32_t devid;
            bool supportsMaps;   // Does this device support map upload/download

            const std::string& getCopyright();

        private:
            void _acquire();
            void _uploadMap(const uint8_t * mapdata, uint32_t size, const char * key);
            void _uploadMap(const char * filename, uint32_t size, const char * key);
            void _queryMap(std::list<Garmin::Map_t>& maps);
            void _downloadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            void _uploadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            void _downloadTracks(std::list<Garmin::Track_t>& tracks);

            void _getDevProperties(Garmin::DevProperties_t& dev_properties);

            void _release();

            Garmin::CSerial * serial;
    };

}
#endif                           //CDEVICE_H
