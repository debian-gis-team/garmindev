/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "config.h"
#include "CDevice.h"

namespace EtrexLegend
{
    static CDevice * device = 0;
}


#ifdef WIN32
#define WIN_EXPORT __declspec(dllexport)
#else
#define WIN_EXPORT
#endif

extern "C" WIN_EXPORT Garmin::IDevice * initEtrexLegend(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexLegend::device == 0) {
        EtrexLegend::device = new EtrexLegend::CDevice();
    }
    EtrexLegend::device->devname = "eTrex Legend";
    EtrexLegend::device->devid = 411;
    return EtrexLegend::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexVista(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexLegend::device == 0) {
        EtrexLegend::device = new EtrexLegend::CDevice();
    }
    EtrexLegend::device->devname = "eTrex Vista";
    EtrexLegend::device->devid = 169;
    return EtrexLegend::device;
}


// I went back-and-forth on where to put the eTrex Classic
// (https://buy.garmin.com/shop/shop.do?cID=167&pID=6403).  Visually,
// and feature capabilities, it is probably close to the eTrex H.  However,
// the eTrex Classic is of the same vintage as the eTrex Legend and eTrex Vista.
// So, as far as firmware (and serial communications go), it is closer to those.
// Therefore, I put it here.
extern "C" WIN_EXPORT Garmin::IDevice * const initEtrexClassic(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexLegend::device == 0) {
        EtrexLegend::device = new EtrexLegend::CDevice();
    }
    EtrexLegend::device->devname = "eTrex";
    EtrexLegend::device->devid = 130;
    EtrexLegend::device->supportsMaps = false;
    return EtrexLegend::device;
}
