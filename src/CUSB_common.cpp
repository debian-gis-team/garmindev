
/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "CUSB.h"
#include "IDevice.h"
#include "Platform.h"

#include <iostream>

using namespace Garmin;
using namespace std;


static int _reduce(Packet_t& incoming, void * ctx);

// This file contains common functions used by both the libusb and the Mac OS X (IOKit) USB code

#define DBG_LINE_SIZE 16

#define DBG

void CUSB::debug(const char * mark, const Packet_t& data)
{
#ifndef DBG
    return;
#endif
    unsigned i;
    uint32_t size;
    unsigned bytes = DBG_LINE_SIZE;
    char buf[DBG_LINE_SIZE + 1];

    memset(buf, 0x20, sizeof(buf));
    buf[DBG_LINE_SIZE] = 0;

    cout << mark << endl << "     ";

    const uint8_t * pData = (const uint8_t*) &data;

    size = gar_endian(uint32_t, data.size);
    if (size > GUSB_MAX_BUFFER_SIZE) {
        cerr << "WARNING! Data size " << data.size << " exceeds buffer size." << endl;
        cerr << "Truncate to " << GUSB_MAX_BUFFER_SIZE << "." << endl;
        size = GUSB_PAYLOAD_SIZE;
    }

    for (i = 0; i < (size + GUSB_HEADER_SIZE); ++i) {
        if (i && !(i % DBG_LINE_SIZE)) {
            cout << " " << buf << endl << "     ";
            memset(buf, 0x20, sizeof(buf));
            buf[DBG_LINE_SIZE] = 0;
            bytes = DBG_LINE_SIZE;
        }

        cout.width(2);
        cout.fill('0');
        cout << hex << (unsigned) pData[i] << " ";

        if (isprint(pData[i]))
            buf[i % DBG_LINE_SIZE] = pData[i];
        else
            buf[i % DBG_LINE_SIZE] = '.';

        --bytes;

    }
    for (i = 0; i < bytes; i++) cout << "   ";
    cout << " " << buf << dec << endl;

}


int CUSB::run_transaction(uint8_t type, uint16_t id, uint8_t * payload, uint32_t p_size, 
        CUSB_cb_t data_cb, void* cb_ctx) {

    Packet_t req, resp;
    int ret=0, ret2;

    if (p_size > GUSB_PAYLOAD_SIZE)
        throw exce_t(errRuntime, "Trying to run a command with an illegal payload size.");

    req.type    = type;
    req.id      = id;
    req.size    = p_size;
    memcpy(req.payload, payload, p_size);

    write(req);

    while (0 < (ret2 = read(resp)) ) {
        ret += ret2;
        if (data_cb) {
            ret2 = data_cb(resp, cb_ctx);
            if( 0 > ret2) ret = ret2;
            if (1 > ret2) break;
        }
    }

    return ret;
}

int CUSB::run_app_command(unsigned command, CUSB_cb_t data_cb, void* cb_ctx)
{
    uint16_t cmd = gar_endian(uint16_t, command);

    return run_transaction(GUSB_APPLICATION_LAYER, Pid_Command_Data, 
            (uint8_t *)&cmd, sizeof(cmd), data_cb, cb_ctx);
}


int CUSB::run_app_command(unsigned command, std::list<Packet_t>& result)
{
    uint16_t cmd = gar_endian(uint16_t, command);
    result.clear();

    return run_transaction(GUSB_APPLICATION_LAYER, Pid_Command_Data, 
            (uint8_t *)&cmd, sizeof(cmd), _reduce, &result);
}

int CUSB::run_product_request(std::list<Packet_t>& result)
{
    result.clear();
    return run_transaction(GUSB_APPLICATION_LAYER, Pid_Product_Rqst, 
            0, 0, _reduce, &result);
}


static int _reduce(Packet_t& incoming, void * ctx)
{
    std::list<Packet_t>* cumulated = reinterpret_cast<std::list<Packet_t>*>(ctx);
    cumulated->push_back(Packet_t());
    Packet_t& n = cumulated->back();
    memcpy(&n, &incoming, sizeof (n));
    return sizeof(incoming);
}
