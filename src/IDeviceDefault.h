/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#ifndef IDEVICEDEFAULT_H
#define IDEVICEDEFAULT_H
#include <pthread.h>
#include <errno.h>

#include "IDevice.h"

namespace Garmin
{
    /// default implementation of a device driver
    /**
        Please refer to IDevice for documentation on the public interface.
        The default implementation of all methods is to throw an exception,
        to signal that a real implementation is missing. If your device lacks
        a ceratin feature simply do not implement the corresponding '_' method.
    */
    class IDeviceDefault : public IDevice
    {
        public:
            IDeviceDefault();
            virtual ~IDeviceDefault();
            /// see IDevice::uploadMap()
            virtual void uploadMap(const uint8_t * mapdata, uint32_t size, const char * key);
            virtual void uploadMap(const char * filename, uint32_t size, const char * key);
            /// see IDevice::queryMap()
            virtual void queryMap(std::list<Map_t>& maps);
            /// see IDevice::downloadWaypoints()
            virtual void downloadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            /// see IDevice::uploadWaypoints()
            virtual void uploadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            /// see IDevice::downloadTracks()
            virtual void downloadTracks(std::list<Garmin::Track_t>& tracks);
            /// see IDevice::uploadTracks()
            virtual void uploadTracks(std::list<Garmin::Track_t>& tracks);
            /// see IDevice::downloadRoutes()
            virtual void downloadRoutes(std::list<Garmin::Route_t>& routes);
            /// see IDevice::uploadRoutes()
            virtual void uploadRoutes(std::list<Garmin::Route_t>& routes);
            /// see IDevice::uploadCustomIcons()
            virtual void uploadCustomIcons(std::list<Garmin::Icon_t>& icons);
            /// see IDevice::screenshot()
            virtual void screenshot(char *& clrtbl, char *& data, int& width, int& height);
            /// see IDevice::setRealTimeMode()
            virtual void setRealTimeMode(bool on);
            /// see IDevice::getRealTimePos()
            virtual void getRealTimePos(Garmin::Pvt_t& pvt);
            /// see IDevice::getDevProperties()
            virtual void getDevProperties(Garmin::DevProperties_t& dev_properties);
            /// see IDevice::getCopyright()
            virtual const std::string& getCopyright();
            /// see IDevice::getLastError()
            virtual const std::string& getLastError();
            /// see IDevice::setPort()
            virtual void setPort(const char * p);

            /// device interface access mutex
            pthread_mutex_t mutex;

        protected:
            virtual void _acquire() = 0;

            virtual void _uploadMap(const uint8_t * mapdata, uint32_t size, const char * key);
            virtual void _uploadMap(const char * filename, uint32_t size, const char * key);
            virtual void _queryMap(std::list<Map_t>& maps);
            virtual void _downloadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            virtual void _uploadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            virtual void _downloadTracks(std::list<Garmin::Track_t>& tracks);
            virtual void _uploadTracks(std::list<Garmin::Track_t>& tracks);
            virtual void _downloadRoutes(std::list<Garmin::Route_t>& routes);
            virtual void _uploadRoutes(std::list<Garmin::Route_t>& routes);
            virtual void _uploadCustomIcons(std::list<Garmin::Icon_t>& icons);
            virtual void _screenshot(char *& clrtbl, char *& data, int& width, int& height);
            virtual void _setRealTimeMode(bool on);
            virtual void _getRealTimePos(Garmin::Pvt_t& pvt);
            virtual void _getDevProperties(Garmin::DevProperties_t& dev_properties);

            virtual void _release() = 0;

            void callback(int progress, int * ok, int * cancel, const char * title, const char * msg);

            /// the copyright information
            std::string copyright;
            /// error message of last error
            std::string lasterror;
            /// serial port string
            std::string port;

            /// device properties
            Garmin::DevProperties_t properties;

    };

    class CMutexLocker
    {
        public:
            CMutexLocker(pthread_mutex_t& mutex)
            : mutex(mutex) {
                if(pthread_mutex_trylock(&mutex) == EBUSY) throw exce_t(errBlocked,"Access is blocked by another function.");
            }

            ~CMutexLocker() {
                pthread_mutex_unlock(&mutex);
            }
        private:
            pthread_mutex_t& mutex;

    };

}
#endif                           //IDEVICEDEFAULT_H
