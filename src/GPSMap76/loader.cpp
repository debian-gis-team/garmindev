/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "config.h"
#include "CDevice.h"

namespace GPSMap76
{
    static CDevice * device = 0;
}


#ifdef WIN32
#define WIN_EXPORT __declspec(dllexport)
#else
#define WIN_EXPORT
#endif

extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap76(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap76::device == 0) {
        GPSMap76::device = new GPSMap76::CDevice();
    }
    GPSMap76::device->devname = "GPSMAP 76";
    GPSMap76::device->devid = 439;
    return GPSMap76::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap76S(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap76::device == 0) {
        GPSMap76::device = new GPSMap76::CDevice();
    }
    GPSMap76::device->devname = "GPSMAP 76S";
    GPSMap76::device->devid = 194;
    return GPSMap76::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initRino120(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap76::device == 0) {
        GPSMap76::device = new GPSMap76::CDevice();
    }
    GPSMap76::device->devname = "Rino 120";
    GPSMap76::device->devid = 264;
    return GPSMap76::device;
}
