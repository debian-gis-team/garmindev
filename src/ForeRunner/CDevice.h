/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#ifndef CDEVICE_H_FR305
#define CDEVICE_H_FR305

#include "IDeviceDefault.h"
#include "CUSB.h"

namespace FR305
{

    class CDevice : public Garmin::IDeviceDefault
    {
        public:
            CDevice();
            virtual ~CDevice();

            std::string devname;
            uint32_t devid;

            const std::string& getCopyright();

        private:
            friend void * rtThread(void *ptr);

            void _acquire();
            void _downloadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            void _uploadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            void _downloadTracks(std::list<Garmin::Track_t>& tracks);
            //void _uploadTracks(std::list<Garmin::Track_t>& tracks);
            void _downloadRoutes(std::list<Garmin::Route_t>& routes);
            void _uploadRoutes(std::list<Garmin::Route_t>& routes);

            void _getDevProperties(Garmin::DevProperties_t& dev_properties);

            void _setRealTimeMode(bool on);
            void _getRealTimePos(Garmin::Pvt_t& pvt);
            void _release();

            Garmin::CUSB * usb;

            /// realtime mode thread
            pthread_t thread;
            /// mutex to serialize any data access
            pthread_mutex_t dataMutex;
            /// keep alive flag for the realtime mode thread
            bool doRealtimeThread;

            Garmin::Pvt_t PositionVelocityTime;

            // Skip Trackpoint if no GPS signal when downloading the tracks
            bool skip_if_no_signal;
    };

}
#endif                           //CDEVICE_H
