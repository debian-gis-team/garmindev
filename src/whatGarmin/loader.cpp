/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "config.h"
#include "CDevice.h"

namespace whatGarmin
{
    static CDevice * device = 0;
}


extern "C" Garmin::IDevice * initwhatGarmin(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(whatGarmin::device == 0) {
        whatGarmin::device = new whatGarmin::CDevice();
    }
    return whatGarmin::device;
}
