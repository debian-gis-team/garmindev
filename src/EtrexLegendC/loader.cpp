/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "config.h"
#include "CDevice.h"

namespace EtrexLegendC
{
    static CDevice * device = 0;
}


#ifdef WIN32
#define WIN_EXPORT __declspec(dllexport)
#else
#define WIN_EXPORT
#endif

extern "C" WIN_EXPORT Garmin::IDevice * initEtrexLegendC(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexLegendC::device == 0) {
        EtrexLegendC::device = new EtrexLegendC::CDevice();
    }
    EtrexLegendC::device->devname = "Etrex Legend C";
    EtrexLegendC::device->devid   = 0x013b;
    return EtrexLegendC::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap60C(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexLegendC::device == 0) {
        EtrexLegendC::device = new EtrexLegendC::CDevice();
    }
    EtrexLegendC::device->devname = "GPSMap60C";
    EtrexLegendC::device->devid   = 0x0123;
    return EtrexLegendC::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap60CS(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexLegendC::device == 0) {
        EtrexLegendC::device = new EtrexLegendC::CDevice();
    }
    EtrexLegendC::device->devname = "GPSMap60CS";
    EtrexLegendC::device->devid   = 0x0123;
    return EtrexLegendC::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap76CS(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexLegendC::device == 0) {
        EtrexLegendC::device = new EtrexLegendC::CDevice();
    }
    EtrexLegendC::device->devname = "GPSMap76CS";
    EtrexLegendC::device->devid   = 0x0123;
    return EtrexLegendC::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexVistaC(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexLegendC::device == 0) {
        EtrexLegendC::device = new EtrexLegendC::CDevice();
    }
    EtrexLegendC::device->devname = "Etrex Vista C";
    EtrexLegendC::device->devid   = 0x013b;
    return EtrexLegendC::device;
}
