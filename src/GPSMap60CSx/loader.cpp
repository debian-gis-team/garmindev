/**********************************************************************************************
    Copyright (C) 2007 Oliver Eichler oliver.eichler@gmx.de

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "config.h"
#include "CDevice.h"

namespace GPSMap60CSx
{
    static CDevice * device = 0;
}


#ifdef WIN32
#define WIN_EXPORT __declspec(dllexport)
#else
#define WIN_EXPORT
#endif

extern "C" WIN_EXPORT Garmin::IDevice * initQuest(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "Quest SYS";
    GPSMap60CSx::device->devid          = 0x0231;
    GPSMap60CSx::device->screenwidth    = 240;
    GPSMap60CSx::device->screenheight   = 160;
    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap76CSx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "GPSMap76CSX";
    GPSMap60CSx::device->devid          = 0x0124;
    GPSMap60CSx::device->screenwidth    = 160;
    GPSMap60CSx::device->screenheight   = 240;
    GPSMap60CSx::device->screenhflip    = false;
    GPSMap60CSx::device->screenvflip    = true;
    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap76Cx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "GPSMap76CX";
    GPSMap60CSx::device->devid          = 0x0124;
    GPSMap60CSx::device->screenwidth    = 160;
    GPSMap60CSx::device->screenheight   = 240;
    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap60CSx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "GPSMap60CSX";
    GPSMap60CSx::device->screenwidth    = 160;
    GPSMap60CSx::device->screenheight   = 240;
    GPSMap60CSx::device->screenhflip    = false;
    GPSMap60CSx::device->screenvflip    = true;
    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap60Cx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "GPSMap60CX";
    GPSMap60CSx::device->devid          = 0x0124;
    GPSMap60CSx::device->screenwidth    = 160;
    GPSMap60CSx::device->screenheight   = 240;

    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initGPSMap60(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "GPSMap60";
    GPSMap60CSx::device->devid          = 0x0134;
    GPSMap60CSx::device->screenwidth    = 160;
    GPSMap60CSx::device->screenheight   = 240;

    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexVentureCx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "eTrex VentureCx";
    GPSMap60CSx::device->devid          = 0x01a5;
    GPSMap60CSx::device->screenwidth    = 176;
    GPSMap60CSx::device->screenheight   = 220;

    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexVistaCx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "eTrex VistaCx";
    GPSMap60CSx::device->devid          = 0x01a5;
    GPSMap60CSx::device->screenwidth    = 176;
    GPSMap60CSx::device->screenheight   = 220;

    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexVentureHC(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "eTrex Venture HC";
    //     GPSMap60CSx::device->devid          = 0x0312;
    GPSMap60CSx::device->screenwidth    = 176;
    GPSMap60CSx::device->screenheight   = 220;
    GPSMap60CSx::device->screenhflip    = true;

    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexVistaHCx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "eTrex Vista HCx";
    GPSMap60CSx::device->devid          = 0x02b6;
    GPSMap60CSx::device->screenwidth    = 176;
    GPSMap60CSx::device->screenheight   = 220;
    GPSMap60CSx::device->screenhflip    = true;

    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexLegendHCx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "eTrex Legend HCx";
    GPSMap60CSx::device->devid          = 0x0694;
    GPSMap60CSx::device->screenwidth    = 176;
    GPSMap60CSx::device->screenheight   = 220;
    GPSMap60CSx::device->screenhflip    = true;

    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexSummitHC(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "eTrex Summit HC";
    //   GPSMap60CSx::device->devid          = 0x0694;
    GPSMap60CSx::device->screenwidth    = 176;
    GPSMap60CSx::device->screenheight   = 220;
    GPSMap60CSx::device->screenhflip    = true;

    return GPSMap60CSx::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexLegendCx(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "eTrex LegendCx";
    GPSMap60CSx::device->devid          = 0x01A5;
    GPSMap60CSx::device->screenwidth    = 176;
    GPSMap60CSx::device->screenheight   = 220;
    GPSMap60CSx::device->screenhflip    = true;
    return GPSMap60CSx::device;
}

extern "C" WIN_EXPORT Garmin::IDevice * initEtrexLegendH(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(GPSMap60CSx::device == 0) {
        GPSMap60CSx::device = new GPSMap60CSx::CDevice();
    }
    GPSMap60CSx::device->devname        = "eTrex Legend H";
    GPSMap60CSx::device->devid          = 957;
    GPSMap60CSx::device->screenwidth    = 160;
    GPSMap60CSx::device->screenheight   = 288;
//    GPSMap60CSx::device->screenhflip    = true;
    return GPSMap60CSx::device;
}
