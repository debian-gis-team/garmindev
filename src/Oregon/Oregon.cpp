/**********************************************************************************************
    Copyright (C) 2010 Thilo Fromm <kontakt@thilo-fromm.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/



#include "config.h"
#include "Oregon.h"
#include "CUSB.h"

#include <iostream>

#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#ifdef WIN32
#include <windows.h>
#endif

#include <unistd.h>

#define GUSB_DATA_AVAILABLE     2
#define GUSB_SESSION_START      5
#define GUSB_SESSION_STARTED    6

#define USB_INTERRUPT_TIMEOUT  3000
#define USB_BULK_TIMEOUT      30000

using namespace Garmin;
using namespace std;

namespace Garmin {

    Oregon::Oregon() : usb(0), devid(0), _rt_pvt_thread_handle(0)
    {
        pthread_mutex_init(&mutex, NULL);
        pthread_mutex_init(&_rt_pvt_thread_mutex, NULL);
        memset(&_rt_pvt_data, 0x00, sizeof(_rt_pvt_data));
    }

    Oregon::~Oregon()
    {

    }


    /* public API implementation see IDeviceDefault.cpp. */



/* -------------------------------
 * private methods
 * ---------------
 */

    void Oregon::_acquire() 
    {
        usb = new Garmin::CUSB();
        usb->open();
        usb->syncup();

        if(strncmp(usb->getProductString().c_str(), devname.c_str(), devname.size()) != 0) {
            string msg = "No " + devname + " unit detected; found \"" 
                + usb->getProductString().c_str() + "\" instead. Please retry to select other device driver.";
            throw exce_t(errSync,msg);
        }
    }
    /* -- */

    void Oregon::_release() 
    {
        if (!usb)
            return;
        usb->close2();
        delete usb;
        usb = 0;
    }
    /* -- */

    void Oregon::_parse_tracks(std::list<Garmin::Track_t>& tracks, std::list<Packet_t> & raw) {
        std::list<Packet_t>::iterator i;
        unsigned total = 0, num_tracks = 0;
        Garmin::Track_t * curr;

        for (i = raw.begin(); i != raw.end(); i++) {
            if(i->id == Pid_Records) 
                total = gar_ptr_load(uint16_t, i->payload);

            if(i->id == Pid_Trk_Hdr) {
                D312_Trk_Hdr_t * hdr = (D312_Trk_Hdr_t*)i->payload;
                tracks.push_back(Track_t());
                curr = &tracks.back();
                *curr << *hdr;
            }

            if(i->id == Pid_Trk_Data) {
                D302_Trk_t * data = (D302_Trk_t*)i->payload;
                if(data->new_trk) {
                    tracks.push_back(Track_t());
                    Track_t& new_track = tracks.back();
                    new_track.color = curr->color;
                    new_track.dspl = curr->dspl;
                    char str[256];
                    snprintf(str, sizeof(str), "%s_%d", curr->ident.c_str(), num_tracks++);
                    new_track.ident = str;
                }

                TrkPt_t pt;
                pt << *data;
                curr->track.push_back(pt);

                if (total && (num_tracks % 50 == 0)) {
                    double progress = (num_tracks * 100.0) / total;
                    callback(progress,0, 0,0,"Transferring tracks.");
                }
            }

            if(i->id == Pid_Xfer_Cmplt)
                break;
        }

        return;
    }
    /* -- */

    void Oregon::_downloadTracks(std::list<Garmin::Track_t>& tracks)
    {

        if(usb == 0) return;

        std::list<Packet_t> ret;
        int cancel = 0; // FIXME: Caancel currently does nothing.
        tracks.clear();

        callback(0,0, &cancel, 0,"Transferring tracks.");

        if (0 > usb->run_app_command(Cmnd_Transfer_Trk, ret) )
            throw exce_t(errRead, "Error downloading track data.");

        _parse_tracks(tracks, ret);

        callback(100,0, &cancel,0,"done");
    }
    /* -- */


    void * _rt_pvt_thread(void * ctx)
    {
        Oregon * dev = (Oregon*) ctx;
        try {
            CMutexLocker lock(dev->mutex);

            dev->_acquire();

            if (0 > dev->usb->run_app_command(Cmnd_Start_Pvt_Data, 0, 0) )
                throw exce_t(errRuntime, "START PVT DATA command to device failed.");

            while (dev->_rt_pvt_thread_handle == pthread_self()) {
                Packet_t pvt;
                if(  (0 < dev->usb->read(pvt)) && (pvt.id == Pid_Pvt_Data) ) {
                    dev->_rt_pvt_data << *(D800_Pvt_Data_t*) pvt.payload;
                    cout << "PVT data received." << endl;
                }
            }

            if (0 > dev->usb->run_app_command(Cmnd_Stop_Pvt_Data, 0, 0) )
                throw exce_t(errRuntime, "STOP PVT DATA command to device failed.");
            dev->_release();
        } catch(exce_t& e) {
            dev->lasterror = "Realtime thread failed. " + e.msg;
        }
        dev->_rt_pvt_thread_handle = 0;
    }
    /* -- */

    void Oregon::_setRealTimeMode(bool mode)
    {
        CMutexLocker lock2(_rt_pvt_thread_mutex);
        if (_rt_pvt_thread_handle && !mode)
            _rt_pvt_thread_handle = 0;
        else if (!_rt_pvt_thread_handle && mode) {
            CMutexLocker lock(mutex);
            _acquire(); // check for device presence. The public IF doesn't do this.
            _release();
            pthread_create(&_rt_pvt_thread_handle, NULL, _rt_pvt_thread, this);
        }
    }
    /* -- */

    void Oregon::_getRealTimePos(Garmin::Pvt_t& pvt)
    {
        if (ESRCH == pthread_kill(_rt_pvt_thread_handle, 0)) {
            /* thread already dead */
            throw exce_t(errRuntime,lasterror);
        }

        pvt = _rt_pvt_data;
    }
    /* -- */


    void Oregon::_getDevProperties(Garmin::DevProperties_t& dev_properties)
    {
        // mark all properties as not having been set to meaningful values
        properties.set.all = (uint32_t) 0;

        if(usb == 0) return;
        Packet_t command;
        Packet_t response;

        // ask for SD Ram capacity
        command.type = GUSB_APPLICATION_LAYER;
        command.id   = Pid_Command_Data;
        command.size = 2;
        *(uint16_t*)command.payload = gar_endian(uint16_t, Cmnd_Transfer_Mem);
        usb->write(command);

        // try to read SD Ram capacity
        uint32_t memory = 0;
        uint16_t tile_limit = 0;
        while(usb->read(response)) {
            if(response.id == Pid_Capacity_Data) {
                tile_limit = gar_ptr_load(uint16_t, response.payload + 2);
                memory = gar_ptr_load(uint32_t, response.payload + 4);
            }
        }
        if(tile_limit == 0) {
            throw exce_t(errRuntime,"Failed to send map: Unable to find the tile limit of the GPS");
        }
        if(memory == 0) {
            throw exce_t(errRuntime,"Failed to send map: Unable to find the available memory of the GPS");
        }

        // add to the properties list
        properties.memory_limit = memory;
        properties.set.item.memory_limit = 1;
        properties.maps_limit = tile_limit;
        properties.set.item.maps_limit = 1;

        // return the properties
        dev_properties = properties;

    }
}

