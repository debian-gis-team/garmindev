/**********************************************************************************************
    Copyright (C) 2010 Thilo Fromm <kontakt@thilo-fromm.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#ifndef OREGON_H
#define OREGON_H
#include <pthread.h>
#include <errno.h>

#include "IDeviceDefault.h"
#include "CUSB.h"


#define OREGON_450_DEVID    896

namespace Garmin
{
    class Oregon : public IDeviceDefault 
    {
        public:
            Oregon();
            virtual ~Oregon();

            std::string devname;
            uint32_t devid;
    
            /* public API spec see IDeviceDefault.cpp. */

        protected:
            Garmin::CUSB* usb;

            void _acquire();
            void _usb_syncup();
            void _parse_tracks(std::list<Garmin::Track_t>& tracks, std::list<Packet_t> & raw);

            friend void * _rt_pvt_thread(void * ctx);
            Pvt_t _rt_pvt_data;
            pthread_t _rt_pvt_thread_handle;
            pthread_mutex_t  _rt_pvt_thread_mutex;

            void _downloadTracks(std::list<Garmin::Track_t>& tracks);
            void _setRealTimeMode(bool on);
            void _getRealTimePos(Garmin::Pvt_t& pvt);
            void _getDevProperties(Garmin::DevProperties_t& dev_properties);

            void _release();


            /// the copyright information
            std::string copyright;
            /// error message of last error
            std::string lasterror;
            /// serial port string
            std::string port;

            /// device properties
            Garmin::DevProperties_t properties;

    };
}
#endif                           //OREGON_H
