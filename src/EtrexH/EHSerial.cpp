/**********************************************************************************************
    Copyright (C) 2008 Frank Seidel <fseidel@suse.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "EHSerial.h"

using namespace Garmin;
using namespace std;

EHSerial::EHSerial(const std::string& port)
: CSerial(port)
{

}


EHSerial::~EHSerial()
{
    EHSerial::close();
}


void EHSerial::syncup()
{
    Packet_t command;
    Packet_t response;
    static int last_response = 2;//only right for current etrex h
    int counter = 0;

    command.type = 0;
    command.id   = Pid_Product_Rqst;
    command.size = 0;

    EHSerial::write(command);

    while(EHSerial::read(response)) {
        if(response.id == Pid_Product_Data) {
            Product_Data_t * pData = (Product_Data_t*)response.payload;
            productId       = pData->product_id;
            softwareVersion = pData->software_version;
            productString   = pData->str;
        }

        if(response.id == Pid_Protocol_Array) {
            Protocol_Data_t * pData = (Protocol_Data_t*)response.payload;
            for(uint32_t i = 0; i < response.size; i += sizeof(Protocol_Data_t)) {
                ++pData;
            }
        }

        ++counter;
        if (last_response && counter == last_response)
            break;
    }
}
