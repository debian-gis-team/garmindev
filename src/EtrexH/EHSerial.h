/**********************************************************************************************
    Copyright (C) 2008 Frank Seidel <fseidel@suse.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "CSerial.h"

#include <string>
#include <ILink.h>
#include "Garmin.h"

#ifndef EHSERIAL_H
#define EHSERIAL_H

namespace Garmin
{
    class EHSerial : public CSerial
    {
        public:
            EHSerial(const std::string& port);
            ~EHSerial();

            void syncup(void);

    };

}
#endif                           //EHSERIAL_H
