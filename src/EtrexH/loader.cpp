/**********************************************************************************************
    Copyright (C) 2007 Frank Seidel <frank@f-seidel.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#include "config.h"
#include "CDevice.h"

namespace EtrexH
{
    static CDevice * device = 0;
}


#ifdef WIN32
#define WIN_EXPORT __declspec(dllexport)
#else
#define WIN_EXPORT
#endif

extern "C" WIN_EXPORT Garmin::IDevice * initEtrexH(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexH::device == 0) {
        EtrexH::device = new EtrexH::CDevice(GARMIN_ETREXH);
    }
    else {
        delete EtrexH::device;
        EtrexH::device = new EtrexH::CDevice(GARMIN_ETREXH);
    }

    return EtrexH::device;
}


extern "C" WIN_EXPORT Garmin::IDevice * initEtrexEuro(const char * version)
{
    if(strncmp(version,INTERFACE_VERSION,5) != 0) {
        return 0;
    }
    if(EtrexH::device == 0) {
        EtrexH::device = new EtrexH::CDevice(GARMIN_ETREXEURO);
    }
    else {
        delete EtrexH::device;
        EtrexH::device = new EtrexH::CDevice(GARMIN_ETREXEURO);
    }

    return EtrexH::device;
}
