/**********************************************************************************************
    Copyright (C) 2007 Frank Seidel <frank@f-seidel.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#ifndef CDEVICE_H
#define CDEVICE_H

#include "EHSerial.h"
#include "IDeviceDefault.h"

#define GARMIN_ETREXH 696
#define GARMIN_ETREXEURO 156

namespace EtrexH
{
    class CDevice : public Garmin::IDeviceDefault
    {
        public:
            CDevice(uint16_t id);
            virtual ~CDevice();

        private:
            void _acquire();
            void _downloadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            void _uploadWaypoints(std::list<Garmin::Wpt_t>& waypoints);
            void _downloadTracks(std::list<Garmin::Track_t>& tracks);
            void _uploadRoutes(std::list<Garmin::Route_t>& routes);
            void _screenshot(char *& clrtbl, char *& data, int& width, int& height);
            void _release();

            Garmin::EHSerial *serial;

            char aClrtbl[0x100 * 4];
            char *pScreen;

            //extension for multimodels (first Etrex Euro)
            uint16_t devId;
    };

}
#endif                           //CDEVICE_H
