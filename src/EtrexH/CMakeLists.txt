
set(CMAKE_VERBOSE_MAKEFILE ON)

set(SRCS
    loader.cpp
    CDevice.cpp
    EHSerial.cpp
)

set(HDRS
    CDevice.h
    EHSerial.h
)

include_directories(../)
add_library(EtrexH SHARED ${SRCS} ${HDRS})
target_link_libraries(EtrexH garmin ${LIBUSB_LIBRARIES} pthread)

set(ALIASES
    EtrexEuro
)

foreach(var ${ALIASES})
    message(" ${var}")
    add_custom_command( TARGET EtrexH
                        POST_BUILD
                        COMMAND ln ARGS -sf libEtrexH${SHARED_LIB_EXT} lib${var}${SHARED_LIB_EXT}
                        WORKING_DIRECTORY ${LIBRARY_OUTPUT_PATH}
                        )
endforeach(var)

