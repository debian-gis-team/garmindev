/**********************************************************************************************
    Copyright (C) 2007 Juan Pablo Daniel Borgna jpdborgna@e-mips.com.ar

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

  Garmin and MapSource are registered trademarks or trademarks of Garmin Ltd.
  or one of its subsidiaries.

**********************************************************************************************/
#ifndef CTCP_H
#define CTCP_H

#include <string>
#include "ILink.h"
#include "Garmin.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#ifndef WIN32
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/time.h>
#endif
#ifdef WIN32
#include <winsock.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#endif

namespace Garmin
{
    class CTcp : public ILink
    {
        public:
            CTcp(const std::string& port);
            virtual ~CTcp();

            /// see ILink::open()
            void open();
            /// see ILink::close()
            void close();
            /// see ILink::read()
            int read(char *data);
            /// see ILink::write()
            void write(char *data);

            int read(Packet_t& data){data=data;return -1;};
            /// see ILink::write()
            void write(const Packet_t& /*data*/){;return;};

            uint16_t getProductId(){return productId;}
            int16_t  getSoftwareVersion(){return softwareVersion;}
            const std::string& getProductString(){return productString;}

        protected:

            struct in_addr * atoaddr(char *address);
            int sock_chars_ready( void);
            time_t time_now( void);
            int sock_read(char * data);
            void sock_write(const char * data);

            // file descripor for socket
            int sock_fd;

            uint16_t productId;
            int16_t  softwareVersion;
            std::string productString;

            std::string port;
    };

}
#endif                           //CSERIAL_H
